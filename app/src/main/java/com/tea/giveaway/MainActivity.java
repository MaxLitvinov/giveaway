package com.tea.giveaway;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.tea.giveaway.board.Cell;
import com.tea.giveaway.board.Game;

public class MainActivity extends AppCompatActivity {

    /**
     * Id's of cells for findViewById() method.
     */
    public static final int[] CELL_IDS = new int[]{
            R.id.a8, R.id.b8, R.id.c8, R.id.d8, R.id.e8, R.id.f8, R.id.g8, R.id.h8,
            R.id.a7, R.id.b7, R.id.c7, R.id.d7, R.id.e7, R.id.f7, R.id.g7, R.id.h7,
            R.id.a6, R.id.b6, R.id.c6, R.id.d6, R.id.e6, R.id.f6, R.id.g6, R.id.h6,
            R.id.a5, R.id.b5, R.id.c5, R.id.d5, R.id.e5, R.id.f5, R.id.g5, R.id.h5,
            R.id.a4, R.id.b4, R.id.c4, R.id.d4, R.id.e4, R.id.f4, R.id.g4, R.id.h4,
            R.id.a3, R.id.b3, R.id.c3, R.id.d3, R.id.e3, R.id.f3, R.id.g3, R.id.h3,
            R.id.a2, R.id.b2, R.id.c2, R.id.d2, R.id.e2, R.id.f2, R.id.g2, R.id.h2,
            R.id.a1, R.id.b1, R.id.c1, R.id.d1, R.id.e1, R.id.f1, R.id.g1, R.id.h1
    };

    /**
     * Using only on first run of Application.
     * Futher uses from {@link Game}
     */
    private Cell[] mCells;
    private Game mGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initCells();
        initGame();
    }

    /**
     * Initialize cells (UI).
     */
    private void initCells() {
        mCells = new Cell[CELL_IDS.length];
        for (int i = 0; i < CELL_IDS.length; i++) {
            mCells[i] = (Cell) findViewById(CELL_IDS[i]);
        }
    }

    /**
     * Initialize Game pattern.
     */
    private void initGame() {
        mGame = new Game(this, mCells);
    }
}
