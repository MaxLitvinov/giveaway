package com.tea.giveaway.board;

import android.view.View;

public class Board {

    private Cell[] mCells;

    public Board(Cell[] cells) {
        mCells = cells;
        initBlackCells();
    }

    public Cell[] getCells() {
        return mCells;
    }

    public Cell getCell(int resId) {
        return mCells[resId];
    }

    public Cell getCell(char letter, int cipher) {
        if (isCellExist(letter, cipher)) {
            for (Cell cell : mCells) {
                if (cell.getLetter() == letter && cell.getCipher() == cipher) {
                    return cell;
                }
            }
        }
        return null;
    }
    
    public boolean isForbiddenCell(Cell cell) {
        return !(isCellExist(cell) && cell.isBlack());
    }

    private boolean isCellExist(Cell cell) {
        for (Cell c : mCells) {
            if (c.getId() == cell.getId()) {
                return true;
            }
        }
        return false;
    }

    public boolean isCellExist(char letter, int cipher) {
        if (letter >= 'a' && letter <= 'h') {
            if (cipher <= 8 && cipher >= 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param cell Current cell.
     * @return Position of current cell in string value.
     */
    public String getPosition(Cell cell) {
        return cell.getContentDescription().toString();
    }

    /**
     * Initialize black cells on which checkers move.
     */
    private void initBlackCells() {
        for (int row = 1, count = 0; row <= 8; row++) {
            if (isOdd(row)) {
                for (int column = 1; column <= 8; column++) {
                    if (!isOdd(column)) {
                        mCells[count].setBlack();
                    }
                    count++;
                }
            } else {
                for (int column = 1; column <= 8; column++) {
                    if (isOdd(column)) {
                        mCells[count].setBlack();
                    }
                    count++;
                }
            }
        }
    }

    private boolean isOdd(int number) {
        return number % 2 != 0;
    }

    public void initCellsClickListener(View.OnClickListener listener) {
        for (Cell cell : getCells()) {
            cell.setOnClickListener(listener);
        }
    }
}
