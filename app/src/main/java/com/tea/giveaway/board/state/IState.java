package com.tea.giveaway.board.state;

import com.tea.giveaway.board.Checker;
import com.tea.giveaway.board.Player;

public interface IState {

    Player whoseMove();
    Checker currentActiveChecker();
}
