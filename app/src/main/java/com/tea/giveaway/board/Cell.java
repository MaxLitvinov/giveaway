package com.tea.giveaway.board;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class Cell extends FrameLayout {

    /**
     * Checkers can move on this cell.
     */
    private boolean mIsBlack;
    private Checker mContainedChecker;

    public Cell(Context context) {
        super(context);
    }

    public Cell(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Cell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public boolean isBlack() {
        return mIsBlack;
    }

    public void setBlack() {
        mIsBlack = true;
    }

    public Checker getContainedChecker() {
        return mContainedChecker;
    }

    public void setChecker(Checker checker) {
        mContainedChecker = checker;
    }

    public boolean isFree() {
        return mContainedChecker != null;
    }

    /**
     * @return Full position. For ex.: a8.
     */
    public String getPosition() {
        return String.valueOf(this.getContentDescription());
    }

    public char getLetter() {
        return getPosition().charAt(0);
    }

    public int getCipher() {
        return Character.getNumericValue(getPosition().charAt(1));
    }
}
