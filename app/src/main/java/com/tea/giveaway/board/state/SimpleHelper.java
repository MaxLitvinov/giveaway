package com.tea.giveaway.board.state;

import com.tea.giveaway.board.Board;
import com.tea.giveaway.board.Cell;
import com.tea.giveaway.board.Checker;

import java.util.ArrayList;
import java.util.List;

public class SimpleHelper implements IMoveHelper {

    private Board mBoard;
    private Checker mChecker;

    public SimpleHelper(Board board) {
        mBoard = board;
    }

    public SimpleHelper(Board board, Checker checker) {
        mBoard = board;
        mChecker = checker;
    }

    @Override
    public Checker getChecker() {
        return mChecker;
    }

    @Override
    public void setChecker(Checker checker) {
        mChecker = checker;
    }

    @Override
    public boolean isCellFree(Cell cell) {
        return cell.isFree();
    }

    @Override
    public boolean hasCellEnemy(Cell cell) {
        if (hasCellAnyChecker(cell)) {
            isInSameTeam(mChecker, cell.getContainedChecker());
        }
        return true;
    }

    @Override
    public Cell getLeftTopCellTowardsChecker(Checker checker) {
        Cell currentCell = checker.getPosition();
        char letter = currentCell.getLetter();
        int cipher = currentCell.getCipher();
        if (mBoard.isCellExist(--letter, ++cipher)) {
            return mBoard.getCell(letter, cipher);
        }
        return null;
    }

    @Override
    public Cell getRightTopCellTowardsChecker(Checker checker) {
        Cell currentCell = checker.getPosition();
        char letter = currentCell.getLetter();
        int cipher = currentCell.getCipher();
        if (mBoard.isCellExist(++letter, ++cipher)) {
            return mBoard.getCell(letter, cipher);
        }
        return null;
    }

    @Override
    public Cell getLeftBottomCellTowardsChecker(Checker checker) {
        Cell currentCell = checker.getPosition();
        char letter = currentCell.getLetter();
        int cipher = currentCell.getCipher();
        if (mBoard.isCellExist(--letter, --cipher)) {
            return mBoard.getCell(letter, cipher);
        }
        return null;
    }

    @Override
    public Cell getRightBottomCellTowardsChecker(Checker checker) {
        Cell currentCell = checker.getPosition();
        char letter = currentCell.getLetter();
        int cipher = currentCell.getCipher();
        if (mBoard.isCellExist(++letter, --cipher)) {
            return mBoard.getCell(letter, cipher);
        }
        return null;
    }

    @Override
    public List<Cell> getMainDiagonal(Checker checker) {
        List<Cell> mainDiagonal = new ArrayList<Cell>();
        while (getLeftTopCellTowardsChecker(checker) != null) {
            mainDiagonal.add(getLeftTopCellTowardsChecker(checker));
        }
        while (getRightBottomCellTowardsChecker(checker) != null) {
            mainDiagonal.add(getRightBottomCellTowardsChecker(checker));
        }
        return mainDiagonal;
    }

    @Override
    public List<Cell> getSecondaryDiagonal(Checker checker) {
        List<Cell> secondaryDiagonal = new ArrayList<Cell>();
        while (getRightTopCellTowardsChecker(checker) != null) {
            secondaryDiagonal.add(getRightTopCellTowardsChecker(checker));
        }
        while (getLeftBottomCellTowardsChecker(checker) != null) {
            secondaryDiagonal.add(getLeftBottomCellTowardsChecker(checker));
        }
        return secondaryDiagonal;
    }

    @Override
    public List<Cell> getPossibleCellsToMove() {
        List<Cell> mainDiagonal = getMainDiagonal(mChecker);
        List<Cell> secondaryDiagonal = getSecondaryDiagonal(mChecker);
        List<Cell> possibleCells = new ArrayList<Cell>();
        possibleCells.addAll(removeNonFreeCells(mainDiagonal));
        possibleCells.addAll(removeNonFreeCells(secondaryDiagonal));
        return possibleCells;
    }

    private boolean hasCellAnyChecker(Cell cell) {
        Checker checker = cell.getContainedChecker();
        return checker != null;
    }

    private boolean isInSameTeam(Checker one, Checker two) {
        return one.getOwner() == two.getOwner();
    }

    private List<Cell> removeNonFreeCells(List<Cell> cells) {
        for (Cell cell : cells) {
            if (!cell.isFree()) {
                cells.remove(cell);
            }
        }
        return cells;
    }
}
