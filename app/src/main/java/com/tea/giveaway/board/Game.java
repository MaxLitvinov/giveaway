package com.tea.giveaway.board;

import android.content.Context;
import android.view.View;

import com.tea.giveaway.R;
import com.tea.giveaway.board.state.IMoveHelper;
import com.tea.giveaway.board.state.IState;
import com.tea.giveaway.board.state.SimpleHelper;

public class Game implements IState {

    private Context mContext;
    private Board mBoard;
    private Player mWhitePlayer;
    private Player mBlackPlayer;
    private Checker mActiveChecker;
    private Player mCurrentPlayer;
    private IMoveHelper mMoveHelper;

    /**
     * Initialize Game pattern.
     * @param cells cells, which get from UI.
     */
    public Game(Context context, Cell[] cells) {
        mContext = context;
        mBoard = new Board(cells);
        mWhitePlayer = initPlayer(0, R.drawable.checker_white);
        mWhitePlayer.setTurnNow(true);
        mBlackPlayer = initPlayer(40, R.drawable.checker_black);
        mBoard.initCellsClickListener(cellsClickListener);
        mCurrentPlayer = mWhitePlayer;
        mActiveChecker = null;
        mMoveHelper = new SimpleHelper(mBoard);
    }

    public Cell[] getCells() {
        return mBoard.getCells();
    }

    public Cell getCell(int resId) {
        return mBoard.getCell(resId);
    }

    public Player getWhitePlayer() {
        return mWhitePlayer;
    }

    public Player getBlackPlayer() {
        return mBlackPlayer;
    }

    public void setCurrentPlayer(Player player) {
        mCurrentPlayer = player;
    }

    /**
     * Initialize player's checkers.
     *
     * @param fromResIdCell cell from which will be put checker to theis positions.
     * @param drawableResId drawable resource for displaying checkers view.
     * @return new {@link Player}
     */
    private Player initPlayer(int fromResIdCell, int drawableResId) {
        Player player = new Player();
        final Checker[] checkers = new Checker[12];
        for (int i = fromResIdCell, count = 0; i < fromResIdCell + checkers.length * 2; i++) {
            Cell cell = mBoard.getCell(i);
            if (cell.isBlack()) {
                checkers[count] = new Checker(mContext, count, drawableResId, cell, player);
                checkers[count].setOnClickListener(checkersClickListener);
                cell.addView(checkers[count]);
                count++;
            }
        }
        player.setCheckers(checkers);
        return player;
    }

    private View.OnClickListener checkersClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Checker checker = (Checker) view;
            toggleActiveChecker(checker);
        }
    };

    private View.OnClickListener cellsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Cell cell = (Cell) view;
            if (activeCheckerExists()) {
                if (!mBoard.isForbiddenCell(cell)) {
                    mActiveChecker.move(cell);
                    togglePlayer();
                }
            }
        }
    };

    @Override
    public Player whoseMove() {
        return mCurrentPlayer;
    }

    @Override
    public Checker currentActiveChecker() {
        return mActiveChecker;
    }

    /**
     * Toggle a new active checker.
     * <br>State of previous active checker will be reset.
     * @param checker
     */
    private void toggleActiveChecker(final Checker checker) {
        if (mCurrentPlayer == checker.getOwner()) {
            if (!activeCheckerExists()) {
                checker.setActive(true);
                mActiveChecker = checker;
            } else if (activeCheckerExists()) {
                toggleChecker(checker);
            }
        }
    }

    /**
     * Method checks if it is exist any active checker.
     */
    private boolean activeCheckerExists() {
        return mActiveChecker != null;
    }

    /**
     * Reset active checker to null.
     */
    private void resetActiveChecker() {
        mActiveChecker.setActive(false);
        mActiveChecker = null;
    }

    /**
     * Toggles checker activity.
     * <br>It is needed for setting/unsetting active state of clicked checker.
     * <br>Previous clicked checker will be unset.
     * @param checker Current checker.
     */
    private void toggleChecker(Checker checker) {
        if (mActiveChecker.getId() == checker.getId()) {
            checker.setActive(false);
            resetActiveChecker();
        } else if (mActiveChecker.getId() != checker.getId()) {
            checker.setActive(true);
            resetActiveChecker();
            mActiveChecker = checker;
        }
    }

    private void togglePlayer() {
        toggleChecker(mActiveChecker);
        if (mCurrentPlayer == mWhitePlayer) {
            mCurrentPlayer = mBlackPlayer;
        } else {
            mCurrentPlayer = mWhitePlayer;
        }
    }
}
