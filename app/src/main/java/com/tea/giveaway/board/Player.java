package com.tea.giveaway.board;

public class Player {

    private boolean mIsTurnNow;
    private Checker[] mCheckers;

    public Player() {
    }

    public Player(Checker[] checkers) {
        mCheckers = checkers;
    }

    public void setTurnNow(boolean isTurnNow) {
        mIsTurnNow = isTurnNow;
    }

    public Checker[] getCheckers() {
        return mCheckers;
    }

    public void setCheckers(Checker[] checkers) {
        mCheckers = checkers;
    }

    public Checker getChecker(int resId) {
        return mCheckers[resId];
    }

    public Checker getChecker(Checker checker) {
        for (Checker ch : mCheckers) {
            if (ch.equals(checker)) {
                return ch;
            }
        }
        return null;
    }
}
