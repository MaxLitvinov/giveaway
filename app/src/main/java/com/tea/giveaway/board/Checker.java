package com.tea.giveaway.board;

import android.content.Context;
import android.widget.ImageButton;

import com.tea.giveaway.R;

public class Checker extends ImageButton {

    private int mId;
    private boolean mActiveState;
    private boolean mAlive;
    private Cell mInclusiveCell;
    private Player mOwningPlayer;

    public Checker(Context context, int id, int drawableResId) {
        super(context);
        mId = id;
        mAlive = true;
        this.setBackgroundResource(drawableResId);
    }

    public Checker(Context context, int id, int drawableResId, Cell cell, Player owningPlayer) {
        super(context);
        mId = id;
        mAlive = true;
        this.setBackgroundResource(drawableResId);
        mInclusiveCell = cell;
        mOwningPlayer = owningPlayer;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public boolean isActive() {
        return mActiveState;
    }

    public void setActive(boolean activationState) {
        if (activationState) {
            this.setImageResource(R.drawable.checker_select);
//            this.setBackgroundResource(R.drawable.checker_white_selected);
        } else {
            this.setImageResource(android.R.color.transparent);
//            this.setBackgroundResource(R.drawable.checker_white);
        }
        mActiveState = activationState;
    }

    public boolean isAlive() {
        return mAlive;
    }

    public void kill() {
        mAlive = false;
        this.setBackgroundResource(0);
    }

    /**
     * @return Current cell wherein checker is.
     */
    public Cell getPosition() {
        return mInclusiveCell;
    }

    public Player getOwner() {
        return mOwningPlayer;
    }

    public void move(Cell to) {
        mInclusiveCell.removeView(this);
        mInclusiveCell = to;
        mInclusiveCell.addView(this);
    }
}
