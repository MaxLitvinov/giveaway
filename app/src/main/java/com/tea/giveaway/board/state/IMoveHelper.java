package com.tea.giveaway.board.state;

import com.tea.giveaway.board.Cell;
import com.tea.giveaway.board.Checker;

import java.util.List;

public interface IMoveHelper {

    Checker getChecker();
    void setChecker(Checker checker);
    boolean isCellFree(Cell cell);
    boolean hasCellEnemy(Cell cell);
    Cell getLeftTopCellTowardsChecker(Checker checker);
    Cell getRightTopCellTowardsChecker(Checker checker);
    Cell getLeftBottomCellTowardsChecker(Checker checker);
    Cell getRightBottomCellTowardsChecker(Checker checker);
    List<Cell> getMainDiagonal(Checker checker);
    List<Cell> getSecondaryDiagonal(Checker checker);
    List<Cell> getPossibleCellsToMove();
}
